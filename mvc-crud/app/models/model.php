<?php

class Database extends PDO{
    public $db;
    public function __construct()
    {
      $dsn = 'mysql:dbname=php_mvc; host=localhost';
      $user = 'root';
      $pass = '';
      $this->db = new PDO($dsn,$user,$pass);
    }
    public function insert(){
      $table = "student";
      $data = array(
                    'name' => 'Hasan', 
                    'roll' => '173002322', 
                    'gpa'  => 'A+'
                  );
      foreach ($data as $key => $value) {
        echo $key .' => '.$value;
        echo '<br>';
      }
      if (!empty($data) && is_array($data)) {
      $keys   = '';
      $value  = '';
      $i      = 0;
      $keys   = implode(',', array_keys($data));
      
      $value  = ":".implode(', :', array_keys($data));

      $sql    =  "INSERT INTO ".$table." (".$keys.") VALUES(".$value.")";

      $query  = $this->db->prepare($sql);

      foreach ($data as $key => $val) 
      {
         $query->bindValue(":".$key,$val);
      }
      $insertData = $query->execute();
      if ($insertData) 
      {
         $lastId = $this->db->lastInsertId();
         return $lastId;
      }
      else
      {
        return false;
      }
    }
    }
  }
  $db = new Database;
  $db->insert();
 ?>
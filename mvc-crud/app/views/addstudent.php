<?php 
  require_once("inc/header.php");

?>

  <div class="card text-center mb-2" style="padding: 10px; background: #D7DBDD;">
    <h1>Dynamic CRUD with OOP & PDO & MVC</h1>
  </div>

  <div class="panel panel-default card mb-2 " style="">
    <div class="panel-heading" style="padding: 10px; background: #D7DBDD;">
      <h2>Add Student<a class="btn btn-success pull-right" style="float: right;" href="http://localhost/mvc-crud/Index/home">Back </a></h2>
    </div>
    <?php 
      /*if (isset($data)) {
        echo "string";
      }*/
      foreach ($data as $key => $value) {
        echo $value;
      }
     ?>
      <div class="form">
        <form style="padding: 10px;" action="http://localhost/mvc-crud/Index/catInsert" method="POST">
          <label>Name</label><br>
          <input type="hidden" value="1" name="addstudent" class="form-control">
          <input type="text" name="name" class="form-control">
          <label>Roll</label><br>
          <input type="number" name="roll" class="form-control">
          <label>GPA</label><br>
          <input type="text" name="gpa" class="form-control">
          <button class="btn btn-info mt-2">Add Student</button>
        </form>
      </div>
  </div>
  <div class="card well text-center" style="padding: 10px; background: #D7DBDD ">
    <h3>Website: https://web.facebook.com/maynuddin.shakil</h3>
      <span class="pull-right">Like us: https://web.facebook.com/maynuddin.shakil</span>
    
  </div>
<?php require_once("inc/footer.php");?>
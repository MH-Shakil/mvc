 <div class="card text-center mb-1  mt-2" style="padding: 10px; background: #D7DBDD;">
    <h1>Dynamic CRUD with OOP & PDO</h1>
  </div>

  <div class="panel panel-default card mb-2" style="">
    <div class="panel-heading" style="padding: 10px; background: #D7DBDD;">
      <h2>Student list<a class="btn btn-success pull-right" style="float: right;" href="http://localhost/mvc-crud/Index/addstudent">Add Student</a></h2>
    </div>
    <table class="table table-striped center" border="1" align="center" style="text-align: center;">
        <tr style="background: green;">
          <th>Id</th>
          <th>Name</th>
          <th>Roll</th>
          <th>GPA</th>
          <th>Action</th>
        </tr>
        <?php 
          $table  = 'student';  
          //$db     = new Database();
  /*   
   $table = 'users';
      $order_by = array('order_by' => 'id DESC');
     
        $selectcond = array('select' => 'name');
        $whereCond = array(
          'where'=>array('id'=>'2', 'name'=>'maynuddin hasan'),
          'return_type'=>'single' 
        );
      
      $stdData = $db->select($table, $order_by);
      */
     
    foreach ($data as $key => $value) { ?>
      <tr>
        <td><?php echo $value['id'] ?></td>
        <td><?php echo $value['name'] ?></td>
        <td><?php echo $value['roll'] ?></td>
        <td><?php echo $value['gpa'] ?></td>
        <td>
          <a class="btn btn-info" href="http://localhost/mvc-crud/Index/updatestudent? action=update & table=student & id=<?php echo $value['id'];?>">Edit</a>
          <a class="btn btn-danger" href="http://localhost/mvc-crud/Index/deleteStudent?action=delete & table=student & id=<?php echo $value['id']; ?>" style="" onclick="return confirm('Are you sure to Delete?')">Delete</a>
        </td>
      </tr>
    <?php
          
        }
      
     ?>
    </table>
  </div>
  <div class="card well text-center" style="padding: 10px; background: #D7DBDD ">
    <h3>Website: https://web.facebook.com/maynuddin.shakil</h3>
      <span class="pull-right">Like us: https://web.facebook.com/maynuddin.shakil</span>
    
  </div>

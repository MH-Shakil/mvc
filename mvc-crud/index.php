<!DOCTYPE html>
<html>
<head>
  <title>CLHG PDO CRUD</title>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="bootstrap.min.css">
  <script type="text/javascript" src="bootstrap.min.js"></script>
  <script type="text/javascript" src="jquery.min.js"></script>
</head>
<body>
<div class="container" >

<?php 
  /*include "system/libs/main.php";
  include "system/libs/Mcontroller.php";
  include "system/libs/database.php";
  include "system/libs/Mmodel.php";
  include "system/libs/load.php";
  require_once("inc/header.php");*/
  /*$main_cntrl = new Main();*/
  require_once("inc/header.php");
  spl_autoload_register(function($class){
    include_once "system/libs/".$class.".php";
  });
  $url = isset($_GET['url']) ? $_GET['url'] : NULL ;
  if ($url !=NULL) {
    $url = rtrim($url,'/');
    $url = explode('/', filter_var($url,FILTER_SANITIZE_URL));
  }else{
    unset($url);
  }
  if (isset($url[0])) {
    $ctrl = $url[0];
    include "app/controllers/".$ctrl.".php";
    $ctrl = new $ctrl;
    if (isset($url[1])) {
      $method = $url[1];
    if (isset($url[2])) {
      $param = $url[1];
      $home = $ctrl->$method($param);
    }
    $home = $ctrl->$method();
    }
  }else{
    include "app/controllers/Index.php";
    $controller = new Index();
    $controller->home();
    /*$table = "student";
      $data = array(
                    'name' => 'Hasan', 
                    'roll' => '173002322', 
                    'gpa' => 'A+'
                  );
      $keys = implode(',',array_keys ($data));
      var_dump($keys);
      $values = ":".implode(', :', array_keys($data));
      var_dump($values);
      $sql = "INSERT INTO $table()";
      //$sql ="INSERT INTO $table() VALUES()";*/
  }
 ?>
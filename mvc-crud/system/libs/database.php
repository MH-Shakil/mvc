<?php 
  /*creat data base database link here*/
  class Database extends PDO{
    public $db;
    public function __construct()
    {
      $dsn = 'mysql:dbname=php_mvc; host=localhost';
      $user = 'root';
      $pass = '';
      return $this->db = new PDO($dsn,$user,$pass);
    }
    public function select($id=Null){
      if (isset($id)) {
        $select = "SELECT * FROM student WHERE id = $id";
      }else{
        $select = "SELECT * FROM student";
      }
      $query  = $this->db->query($select);
      $data = $query->fetchAll();
      return $data;
    }
    public function insert($table, $data){
      if (!empty($data) && is_array($data)) {
      $keys   = '';
      $value  = '';
      $i      = 0;
      $keys   = implode(',', array_keys($data));
      
      $value  = ":".implode(', :', array_keys($data));

      $sql    =  "INSERT INTO ".$table." (".$keys.") VALUES(".$value.")";

      $query  = $this->db->prepare($sql);

      foreach ($data as $key => $val) 
      {
         $query->bindValue(":".$key,$val);
      }
      $insertData = $query->execute();
      if ($insertData) 
      {
         $lastId = $this->db->lastInsertId();
         return $lastId;
      }
      else
      {
        return false;
      }
    }
    }
    public function delete($id, $table){
      $sql = "DELETE FROM $table WHERE id = $id";
      $query = $this->db->prepare($sql);
      $delete = $query->execute();
      return $delete;
    }
    public function  update($table,$data=array(),$cond)
  {
    $keyvalue   = '';
    $wherecond  = '';
    $i =  0;
    foreach ($data as $key => $value) {
      $add =  ($i > 0)?',':'';
      $keyvalue .="$add"."$key=:$key";
      $i++; 
    }
    if (!empty($cond) &&  is_array($cond)) 
    {
      $wherecond  .= " WHERE ";
      $i = 0;
      foreach ($cond as $key => $val) {
        $add = ($i > 0)?' , ':'';
        $wherecond  .= "$add"."$key=:$key";
        $i++;
      }
    }
    $sql = "UPDATE ".$table. "SET " .$keyvalue. $wherecond ;
    $query = $this->db->prepare($sql);
    foreach ($data as $key => $val) 
    {
      $query->bindValue(":$key",$val);
    }
    foreach ($cond as $key => $value) 
    {
      $query->bindValue(":$key",$value);
    }
    $update = $query->execute();
    return $update;
  }
/*    public function update($id, $table, $stdData){
      $sql = "DELETE ";
      if (isset(($_POST['editstudent']))) {
        $id = $_POST['id'];
        echo $_POST['name']
        $update     = $db->update($table,$updateData,$condition);
        if ($update) {
          echo "successfully updated";
          header('location:'.$home_url);

        }
        else
        {
          echo " not updated";
        }   
      }
    }*/
  }
?>
<?php 
 class Load{
  public function __construct(){}
  public function view($filename, $data = array()){
    include 'app/views/'.$filename.'.php';
  }
  public function model($CatModel)
  {
    include 'app/models/'.$CatModel.'.php';
    return new $CatModel();
  }
 }

 ?>